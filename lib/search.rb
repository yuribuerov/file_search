require 'moneta'

class Search
  def initialize(words)
    @words = words.downcase.gsub(/[^a-z\s]/i, '').split(' ')
  end

  def search
    storage = Moneta.new(:LevelDB, dir: 'bin/index')
    results = []

    @words.each do |word|
      if storage.key?(word)
        results << { word: word, status: "found in #{storage[word].join(',').gsub(/[^a-z0-9.,=>]/i, '')}" }
      else
        results << { word: word, status: 'was not found' }
      end
    end

    storage.close
    results
  end
end
