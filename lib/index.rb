require 'ruby-filemagic'
# require 'mime/types'
require 'moneta'
require 'words_frequency'

class Index
  def initialize(file_path)
    @file_path = file_path

    raise StandardError, 'Wrong file type, or file does not exist' unless text_file?
  end

  def self.index_file(*args)
    index = new(*args)

    index.index_file
  end

  def index_file
    f = File.open(@file_path, 'r')

    f.each_line do |line|
      line.downcase.gsub(/[^a-z\s]/i, '').split(' ').each do |word|
        create_record(word)
      end
    end

    f.close
    storage.close
  end

  private

  def text_file?
    fm = FileMagic.new(FileMagic::MAGIC_MIME)
    fm.file(@file_path).split(';').first == 'text/plain'
  end

  def words_frequency
    @word_frequency ||= WordsFrequency.new(@file_path).calculate
  end

  def file_name
    @file_name ||= File.basename(@file_path)
  end

  def storage
    @storage ||= Moneta.new(:LevelDB, dir: 'bin/index')
  end

  def create_record(word)
    storage[word] = [] unless storage.key?(word)
    word_params = storage[word]

    frequency = words_frequency[word]
    word_params << { file_name => frequency } unless word_params.detect { |h| h[file_name] }
    storage[word] = word_params
  end
end
