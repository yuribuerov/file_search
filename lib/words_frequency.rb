class WordsFrequency
  def initialize(file_path)
    @file_path = file_path
  end

  def calculate
    result = Hash.new(0)
    f = File.open(@file_path, 'r')

    f.each_line do |line|
      line.downcase.gsub(/[^a-z\s]/i, '').split(' ').each do |word|
        result[word] += 1
      end
    end

    f.close
    result
  end
end
