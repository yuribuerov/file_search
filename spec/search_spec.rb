require 'spec_helper'

describe Search do
  let(:expected_result) do
    [
      { word: 'one', status: 'found in kafka.txt=>1' },
      { word: 'morning', status: 'found in kafka.txt=>4,morning.txt=>1' },
      { word: 'buzzword', status: 'was not found' }
    ]
  end

  before do
    Index.index_file('spec/fixtures/kafka.txt')
    Index.index_file('spec/fixtures/morning.txt')
  end

  it 'returns expected result' do
    expect(described_class.new("one morning buzzword").search).to eq(expected_result)
  end
end
