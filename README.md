# System dependencies
leveldb
libmagic

# OSX
brew install leveldb

brew install libmagic


# Usage
```
Application Options:
-i, --index=  Path to file
-s, --search= List of words to search

Help Options:
-h, --help

Example:
bin/wfs -i some/path/
bin/wfs -s 'some words'
```
